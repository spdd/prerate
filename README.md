# Библиотека с диалогом проголосовать #

Первое отображение диалога через 7 дней после первого запуска, если пользователь нажал напомнить позже, то через 3 дня.
Диалог отображается только при наличии сети(если нет сети, то нет смысла показывать - пользователь не сможет проголосовать)

### Добавление в ваш проект ###
1. Допишите в onResume вашей главной Activity(в которой будет появляться диалог)

```
#!android

@Override
    protected void onResume() {
        super.onResume();
        PreRate.init(this,"email@mail.ru","Отзыв по приложению").showIfNeed();
    }
```


При инициализации  устанавалиется email куда отпоравлять отзыв, если он ниже 5 звезд и заголовок письма.

2. Допишите в onDestroy

```
#!android

@Override
    protected void onDestroy() {
        super.onDestroy();
        PreRate.clearDialogIfOpen();
    }
```

Картинка как это работает [ссылка](https://docs.google.com/drawings/d/1ZCM5ZOJYOdAB8PXSXoILxb1N-XVNDMb0TKIChsTWXRU/pub?w=928&h=1890)